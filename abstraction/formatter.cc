#ifndef __RELATION_FORMATTER_HH_
#define __RELATION_FORMATTER_HH_

#include <numeric>
#include <string>
#include <vector>

#include "bdd-abstraction.hh"

namespace CuddAbstraction {
///////////////
// Formatter //
///////////////

Formatter::Formatter(const Relation& r, std::ostream& os, DomainGroup* dg,
                     int maxWidth)
    : rel_(r)
    , os_(os)
    , dg_(dg)
    , maxWidth_(maxWidth)
    , widths_(r.arity(), 0) {
  widthsForPrinting();
}

void Formatter::widthsForPrinting(void) {
  for (size_t i = 0; i < rel_.arity(); i++) {
    const auto& att = rel_.schema().asColumnDomains()[i];
    if (dg_ && dg_->isThereMappingFor(att)) {
      widths_[i] = std::max(att.name().length(), dg_->getWidth(att));
    } else {
      // TODO: printWidth method of an attribute domain should be part of the
      // formatter and not of the attribute domain.
      widths_[i] = std::max(att.name().length(), att.domain()->printWidth());
    }
  }
  // Take into account the maximum width of the table
  int tw = totalLength();
  if (tw > maxWidth_) {
    int maxColWidth = maxWidth_ / widths_.size();
    for (size_t i = 0; i < rel_.arity(); i++)
      widths_[i] = std::min(widths_[i], maxColWidth);
  }
  // Add one character to separate the values in the columns from the separator
  for (size_t i = 0; i < widths_.size(); i++) {
    widths_[i] += 1;
  }
}

int Formatter::totalLength(void) const {
  int totalLength = std::accumulate(widths_.begin(), widths_.end(), 0);
  totalLength += widths_.size() + 1; // Counts the separators
  return totalLength;
}

std::vector<std::string> Formatter::identityDecode(const NumericTuple& t) {
  size_t arity = t.size();
  std::vector<std::string> decoded(arity);
  for (size_t i = 0; i < arity; i++)
    decoded[i] = std::to_string(t[i]);
  return decoded;
}
/**
 * @brief Returns a copy of @a s left aligned in a space of size @a width
 */
std::string left_align(const std::string& s, size_t width) {
  std::string result(width, ' ');
  if (s.size() < width) { // padding
    result.replace(0, s.size(), s);
    return result;
  } else if (s.size() > width) { // truncate
    result.replace(0, width, s, 0, width);
    result.replace(result.end() - 3, result.end(), "...");
    return result;
  }
  return s;
}

Formatter::Decoder Formatter::getDecoder(void) const {
  assert(dg_ && "Expecting valid domain group");
  return [&](const NumericTuple& et) -> std::vector<std::string> {
    std::vector<std::string> dt(rel_.schema().arity());
    for (size_t i = 0; i < rel_.schema().arity(); i++) {
      const Attribute& att = rel_.schema().asColumnDomains()[i];
      if (dg_->isThereMappingFor(att)) {
        dt[i] = dg_->getValueEncodedBy(et[i], att);
      } else {
        dt[i] = std::to_string(et[i]);
      }
    }
    return dt;
  };
}

void Formatter::decodeAndPrintTuples(const SpaceManager& home, Decoder dec,
                                     bool newLine) const {
  const auto& tuples = rel_.asTuples(home);
  for (const NumericTuple& t : tuples) {
    auto dt = dec(t);
    for (size_t i = 0; i < rel_.arity(); i++)
      os_ << '|' << left_align(dt[i], widths_[i]);
    os_ << '|';
    if (newLine)
      os_ << '\n';
  }
}

void Formatter::printSchema(void) const {
  for (size_t i = 0; i < rel_.arity(); i++) {
    const auto& att = rel_.schema().asColumnDomains()[i];
    os_ << '|' << left_align(att.name(), widths_[i]);
  }
  os_ << '|' << '\n';
}

void Formatter::printSeparator(char c) const {
  os_ << '|';
  for (size_t i = 0; i < widths_.size(); i++) {
    std::string s(widths_[i], c);
    os_ << s;
    if (i == widths_.size() - 1)
      os_ << '|';
    else
      os_ << '+';
  }
  os_ << '\n';
}

void Formatter::printTuples(const SpaceManager& home) const {
  if (!dg_)
    decodeAndPrintTuples(home, &identityDecode);
  else
    decodeAndPrintTuples(home, getDecoder());
}

void Formatter::print(const SpaceManager& home) const {
  printSeparator('-');
  printSchema();
  printSeparator('-');
  printTuples(home);
  // TODO: Print some relation statistics?
  printSeparator('-');
  os_ << '\n';
}

void print(const SpaceManager& home, const Relation& rel, std::ostream& os,
           DomainGroup* dg, int maxWidth) {
  Formatter f(rel, os, dg, maxWidth);
  f.print(home);
}
}

#endif