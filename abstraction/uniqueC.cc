#include "unique.h"

#include <cuddInt.h>

/*
  This is a copy of the function with the same name at the end of
  cuddBddAbs.c. the reason it is here is because that function is
  not in the public interface.
*/
static int bddCheckPositiveCube(DdManager* manager, DdNode* cube) {
  if (Cudd_IsComplement(cube))
    return (0);
  if (cube == DD_ONE(manager))
    return (1);
  if (cuddIsConstant(cube))
    return (0);
  if (cuddE(cube) == Cudd_Not(DD_ONE(manager))) {
    return (bddCheckPositiveCube(manager, cuddT(cube)));
  }
  return (0);
}

DdNode* cuddBddExistAbstractRecur(DdManager* manager, DdNode* f, DdNode* cube);

// Returns the level of the top variable in @a f
int var2level(DdManager* home, DdNode* f) {
  return Cudd_ReadPerm(home, Cudd_NodeReadIndex(f));
}

// Returns the variable at a given level
DdNode* topVar(DdManager* home, DdNode* f) {
  return Cudd_ReadVars(home, Cudd_NodeReadIndex(f));
}

DdNode* uniqueAbstractRec(DdManager* home, DdNode* f, DdNode* cube) {
  // TODO: should I add the following?
  // satLine(home)

  // Constants
  DdNode* one = Cudd_ReadOne(home);
  DdNode* zero = Cudd_Not(one);

  // Base case: the cube is completely consumed and the abstraction finishes.
  if (cube == one)
    return f;

  // Base case: f is finished but there are still variables in the cube to
  // abstract. This happens when a variable is a don't care in the BDD and
  // nothing along this path can be unique. Because either value of the variable
  // is admissible in the function. This case can also happen when trying to
  // abstract a variable that does not belongs to f. It is up to user's
  // responsibility to avoid that.
  if ((f == zero || f == one) && cube != one)
    return zero;

  // From now on f is not constant
  int topf = var2level(home, f);
  int topc = var2level(home, cube);

  // Base case: abstracting a variable that does not belong to f.
  if (topf > topc)
    return zero;

  DdNode* F = Cudd_Regular(f);

  // Check the cache
  if (F->ref != 1) {
    DdNode* result = cuddCacheLookup2(home, uniqueAbstract, f, cube);
    if (result != NULL)
      return result;
  }

  DdNode* T = Cudd_T(F);
  DdNode* E = Cudd_E(F);
  if (f != F) {
    T = Cudd_Not(T);
    E = Cudd_Not(E);
  }

  if (topf == topc) {
    // Recursive case: We are in the case in which the current top of f has to
    // be abstracted.

    DdNode* uniqueThen = uniqueAbstractRec(home, T, Cudd_T(cube));
    if (uniqueThen == NULL)
      return NULL;
    // if (uniqueThen == one) {
    //   // printf("Uniquethen is 1\n");
    // }
    Cudd_Ref(uniqueThen);

    DdNode* existElse = cuddBddExistAbstractRecur(home, E, Cudd_T(cube));
    if (existElse == NULL) {
      Cudd_IterDerefBdd(home, uniqueThen);
      return NULL;
    }
    Cudd_Ref(existElse);

    DdNode* resultThen = Cudd_bddAnd(home, uniqueThen, Cudd_Not(existElse));
    if (resultThen == NULL) {
      Cudd_IterDerefBdd(home, uniqueThen);
      Cudd_IterDerefBdd(home, existElse);
      return NULL;
    }
    // if (resultThen == one) {
    //   // printf("resultThen = 1\n");
    //   if (F->ref != 1)
    //     cuddCacheInsert2(home, uniqueAbstract, f, cube, one);
    //   Cudd_IterDerefBdd(home, uniqueThen);
    //   Cudd_IterDerefBdd(home, existElse);
    //   return one;
    // }
    Cudd_Ref(resultThen);
    Cudd_IterDerefBdd(home, uniqueThen);
    Cudd_IterDerefBdd(home, existElse);

    // From this point on uniqueThen and existElse should not be used.

    DdNode* existThen = cuddBddExistAbstractRecur(home, T, Cudd_T(cube));
    if (existThen == NULL) {
      Cudd_IterDerefBdd(home, resultThen);
      return NULL;
    }
    Cudd_Ref(existThen);

    DdNode* uniqueElse = uniqueAbstractRec(home, E, Cudd_T(cube));
    if (uniqueElse == NULL) {
      Cudd_IterDerefBdd(home, resultThen);
      Cudd_IterDerefBdd(home, existThen);
      return NULL;
    }
    // if (uniqueElse == one) {
    // printf("UniqueElse is one\n");
    // }
    Cudd_Ref(uniqueElse);

    DdNode* resultElse = Cudd_bddAnd(home, uniqueElse, Cudd_Not(existThen));
    if (resultElse == NULL) {
      Cudd_IterDerefBdd(home, resultThen);
      Cudd_IterDerefBdd(home, existThen);
      Cudd_IterDerefBdd(home, uniqueElse);
      return NULL;
    }
    // if (resultElse == one) {
    // printf("resultElse = 1\n");
    // }
    Cudd_Ref(resultElse);
    Cudd_IterDerefBdd(home, existThen);
    Cudd_IterDerefBdd(home, uniqueElse);

    // From this point on existThen and uniqueElse should not be used

    DdNode* result = Cudd_bddOr(home, resultThen, resultElse);
    if (result == NULL) {
      Cudd_IterDerefBdd(home, resultThen);
      Cudd_IterDerefBdd(home, resultElse);
      return NULL;
    }
    Cudd_Ref(result);
    Cudd_IterDerefBdd(home, resultThen);
    Cudd_IterDerefBdd(home, resultElse);

    // if (resultElse == one || resultThen == one) {
    // if (result == one)
    // printf("The conjunction is one\n");
    // }
    // Store the result in the cache
    if (F->ref != 1) {
      cuddCacheInsert2(home, uniqueAbstract, f, cube, result);
    }
    Cudd_Deref(result);
    return result;
  } else {
    // Recursive case: We are int he case in which the current top does not need
    // to be abstracted.
    DdNode* uniqueThen = uniqueAbstractRec(home, T, cube);
    if (uniqueThen == NULL)
      return NULL;
    Cudd_Ref(uniqueThen);

    DdNode* uniqueElse = uniqueAbstractRec(home, E, cube);
    if (uniqueElse == NULL) {
      Cudd_IterDerefBdd(home, uniqueThen);
      return NULL;
    }
    Cudd_Ref(uniqueElse);

    DdNode* result = Cudd_bddIte(home, topVar(home, F), uniqueThen, uniqueElse);
    if (result == NULL) {
      Cudd_IterDerefBdd(home, uniqueThen);
      Cudd_IterDerefBdd(home, uniqueElse);
      return NULL;
    }

    Cudd_Ref(result);
    Cudd_IterDerefBdd(home, uniqueThen);
    Cudd_IterDerefBdd(home, uniqueElse);
    Cudd_Deref(result);

    // Store the result in the cache
    if (F->ref != 1)
      cuddCacheInsert2(home, uniqueAbstract, f, cube, result);

    return result;
  }
}

DdNode* uniqueAbstract(DdManager* home, DdNode* f, DdNode* cube) {
  if (bddCheckPositiveCube(home, cube) == 0) {
    printf("The provided cube is not a cube\n");
    return NULL;
  }

  DdNode* res;
  do {
    home->reordered = 0;
    res = uniqueAbstractRec(home, f, cube);
  } while (home->reordered == 1);

  return res;
}
