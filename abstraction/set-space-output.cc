#include "set-space-decl.hh"
#include "set-space-basic.hh"

#include <cassert>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

namespace CuddAbstraction {

namespace internal {
typedef std::set<DdNode*> NodeSet;

void visitBDDNodesHelper(DdNode* f, NodeSet& visited, NodeVisitor visit) {
  assert(f && "Unexpected function");
  assert(!Cudd_IsComplement(f) && "Complemented function!");

  if (visited.count(f) > 0) return;

  visited.insert(f);
  visit(f);

  if (Cudd_IsConstant(f)) return;

  DdNode* t = Cudd_T(f);
  visitBDDNodesHelper(t, visited, visit);

  DdNode* e = Cudd_Regular(Cudd_E(f));
  visitBDDNodesHelper(e, visited, visit);
}
}

void visitBDDNodes(DdNode* f, NodeVisitor visit) {
  std::set<DdNode*> visited;
  internal::visitBDDNodesHelper(Cudd_Regular(f), visited, visit);
}

namespace internal {
typedef std::unordered_map<int, std::vector<DdNode*>> LevelsType;

int level(DdManager* manager, DdNode* f) {
  return Cudd_ReadPerm(manager, Cudd_NodeReadIndex(f));
}

LevelsType findLevels(DdManager* manager, const NodeSet& nodes) {
  LevelsType levels;
  for (const auto& node : nodes) {
    int l = level(manager, node);
    levels[l].push_back(node);
  }
  return levels;
}
std::string quoted(const std::string& val) {
  return std::string("\"") + val + "\"";
}

template <typename T>
std::string quoted(const T& val) {
  return std::string("\"") + std::to_string(val) + "\"";
}

template <typename T>
std::string quoted(T* val) {
  std::stringstream ss;
  ss << "\"" << val << "\"";
  return ss.str();
}

void functionNode(std::ostream& os, DdNode* f, const std::string& fname) {
  const char* dotted = " style=dotted ";
  const char* solid = " style=solid ";
  const char* box = " shape=box ";

  os << quoted(fname) << " -> " << quoted(Cudd_Regular(f)) << " [";

  if (Cudd_IsComplement(f))
    os << dotted;
  else
    os << solid;

  os << "," << box << "];\n";
}

void internalEdges(std::ostream& os, NodeSet& visited) {
  const char* dotted = " [style = dotted]; ";
  const char* solid = " [style = solid]; ";
  const char* dashed = " [style = dashed]; ";

  for (auto& node : visited) {
    if (Cudd_IsConstant(node)) {
      // Do not try to print the edges outgoing from a constant nodes
      continue;
    }

    os << quoted(node) << " -> " << quoted(Cudd_T(node)) << solid << "\n";
    if (Cudd_IsComplement(Cudd_E(node)))
      os << quoted(node) << " -> " << quoted(Cudd_Regular(Cudd_E(node)))
         << dotted << "\n";
    else
      os << quoted(node) << " -> " << quoted(Cudd_E(node)) << dashed << "\n";
  }
}

void internalNodes(std::ostream& os, DdManager* mgr,
                   const SpaceManager::LevelNamesMapType& names, NodeSet& nodes,
                   const LevelsType& levels) {
  for (const auto& l : levels) {
    os << "{rank = same; ";
    for (const auto& n : l.second) os << quoted(n);
    os << "}\n";
  }

  for (auto& node : nodes)
    if (Cudd_IsConstant(node))
      os << quoted(node)
         << " [label =" << quoted(static_cast<int>(Cudd_V(node)))
         << ", shape=box];\n";
    else {
      auto it = names.find(level(mgr, node));
      assert(it != names.end());
      const std::string& name = it->second;
      os << quoted(node) << "[label =" << quoted(name) << "];\n";
    }
}
}

SpaceManager::LevelNamesMapType SpaceManager::extractLevels(DdNode* f) const {
  using namespace internal;

  LevelNamesMapType levelsToNames;
  auto visit = [&](DdNode* n) -> void {
    if (!Cudd_IsConstant(n)) levelsToNames[level(n)] = std::string();
  };

  visitBDDNodes(Cudd_Regular(f), visit);
  return levelsToNames;
}

void SpaceManager::toDot(std::ostream& os, DdNode* f,
                         const std::string& funcName,
                         const LevelNamesMapType& names, float width,
                         float height) const {
  using namespace internal;

  /// Get the nodes in fun
  NodeSet nodes;
  auto collectNode = [&nodes](DdNode* n) -> void { nodes.insert(n); };
  visitBDDNodes(f, collectNode);

  // Group the nodes by level
  auto levels = findLevels(getManager(), nodes);

  os << "digraph " << quoted("DD") << "{\n"
     << "center=true;\n"
     << "edge [dir=none];\n"
     << "size=\"" << width << "," << height << "\";\n";

  functionNode(os, f, funcName);
  internalNodes(os, getManager(), names, nodes, levels);
  internalEdges(os, nodes);

  os << "}";
}

void SpaceManager::toDot(const std::string& fileName, DdNode* f,
                         const std::string& funcName,
                         const LevelNamesMapType& names, float width,
                         float height) const {
  std::ofstream os(fileName);
  if (!os) {
    std::cerr << "Error opening " << fileName << " for writing\n";
    return;
  }
  toDot(os, f, funcName, names, width, height);
}
}