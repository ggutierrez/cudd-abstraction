#ifndef __INTEGER_DATUM_HH__
#define __INTEGER_DATUM_HH__

#include "integer-datum-decl.hh"

#include <climits>
#include <limits>
#include <algorithm>

namespace CuddAbstraction {

//////////////////
// Constructors //
//////////////////
inline IntegerDatum::IntegerDatum(void)
    : storage_() {}

inline IntegerDatum::IntegerDatum(int x)
    : storage_(x) {
  assert(Limits::valid(x));
}

inline IntegerDatum::IntegerDatum(const IntegerDatum& x)
    : storage_(x.storage_) {}

inline IntegerDatum::~IntegerDatum(void) {}

///////////////////////////////
// Representation operations //
///////////////////////////////
inline size_t IntegerDatum::numBits(void) const {
  int n = static_cast<int>(*this);
  return Limits::bitsRequiredToRepresent(n);
}

inline IntegerDatum::operator int(void) const {
  /*
      It is safe to cast in this case because on construction we guarantee that
      only positive integers are encoded.
   */
  return static_cast<int>(storage_.to_ulong());
}

inline bool IntegerDatum::getBit(int n) const {
  assert(Limits::valid_bit(n) && "Invalid bit");
  return storage_[n];
}

inline void IntegerDatum::setBit(int n) {
  assert(Limits::valid_bit(n) && "Invalid bit");
  storage_.set(n);
}

inline void IntegerDatum::clearBit(int n) {
  assert(Limits::valid_bit(n) && "Invalid bit");
  storage_.reset(n);
}

inline std::string IntegerDatum::encoding(void) const {
  return storage_.to_string();
}

inline bool IntegerDatum::operator<=(const IntegerDatum& n) const {
  return static_cast<int>(*this) <= static_cast<int>(n);
}

inline std::ostream& operator<<(std::ostream& os, const IntegerDatum& d) {
  os << static_cast<int>(d);
  return os;
}

inline std::ostream& operator<<(std::ostream& os, const NumericTuple& t) {
  os << "[";
  std::copy(t.begin(), t.end(), std::ostream_iterator<IntegerDatum>(os, " "));
  os << "]";
  return os;
}
}

#endif
