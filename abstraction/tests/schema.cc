#include <iostream>
#include <gtest/gtest.h>

#include "bdd-abstraction.hh"

using namespace std;
using namespace CuddAbstraction;

TEST(Attribute, creation) {
  IntegerDatum x(4);
  EXPECT_EQ(x.numBits(), 3u);

  SpaceManager home;
  {
    /*
    // TODO: This test was disabled when the notion of domain was added. It has
    // to be enabled again in a meaningfull way.
    AttributeDomain ad = makeDomain();
    Attribute d(home,{0,1,2},ad);
    BDD encX = d.asBDD(home,x);
    cubeVisitor(encX,[&home](int *x) -> void {
      for (int i = 0; i < home.ReadSize(); i++) {
        std::cout << "Var [" << i << "] -> " << x[i] << std::endl;
      }
    });
    */
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Schema, incrementalCreation) {
  SpaceManager home;
  {

    Attribute c0(home, makeDomain(4)), c1(home, makeDomain(4)),
        c2(home, makeDomain(4));

    Attribute d0(home, makeDomain(4)), d1(home, makeDomain(4));

    Attribute e0(home, makeDomain(4));

    Schema b(home);
    EXPECT_TRUE(b.isEmpty());
    EXPECT_EQ(b.reprSize(), 0);

    Schema c(home, {c0, c1, c2});
    EXPECT_EQ(3u, c.arity());
    EXPECT_EQ(c0.reprSize() + c1.reprSize() + c2.reprSize(), c.reprSize());

    Schema mixed(home, {c0, d0, e0});
    EXPECT_EQ(3u, mixed.arity());
    EXPECT_EQ(c0.reprSize() + d0.reprSize() + e0.reprSize(), mixed.reprSize());

    Schema cincr(home);
    EXPECT_TRUE(cincr.isEmpty());
    cincr.add(c0);
    EXPECT_FALSE(cincr.isEmpty());
    EXPECT_EQ(1u, cincr.arity());
    cincr.add(c0);
    EXPECT_EQ(1u, cincr.arity());
    cincr.add(c1);
    cincr.add(c2);
    EXPECT_EQ(3u, cincr.arity());
    EXPECT_TRUE(cincr.sameAs(c));

    Schema incrmixed(home);
    incrmixed.add(c0);
    incrmixed.add(d0);
    incrmixed.add(e0);
    EXPECT_EQ(3u, incrmixed.arity());
    EXPECT_TRUE(incrmixed.sameAs(mixed));
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Schema, creation) {
  SpaceManager home;
  {
    Attribute col0(home, makeDomain(4));
    Attribute col1(home, makeDomain(4));
    Attribute col2(home, makeDomain(4));

    Schema rd0(home, {col0, col1, col2});
    // Schema rd1({col0});
    Schema rd2(home, {col0, col1});

    // EXPECT_EQ(rd0.arity(),3);
    // EXPECT_EQ(rd1.arity(),1);
    // EXPECT_EQ(rd2.arity(),2);

    BDD a = rd2.asBDD(home, {0, 2});
    BDD b = rd2.asBDD(home, {2, 2});

    // BDD b = rd2.asBDD(home,{0,3});

    auto decoded = rd2.asTuples(home, a | b);
    EXPECT_EQ(decoded.size(), 2u);
    // for (const auto& t : decoded)
    //  cout << t << std::endl;
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Schema, interleaved) {
  SpaceManager home;
  {

    /**
     * TODO: I have to think about this again.
     *
     * - A translator is not actually just a translator, it is a domain and
     *   hence it cannot be changed. It has to be associated to an attribute at
     *   construction.
     */
    // Attribute col0, col1;
    // initializeDomains(home,16,col0,col1);
    // col0.name("Col0");
    // col0.translator(new Translator());
    // col1.name("Col1");
    // col1.translator(new Translator());

    // Schema d(home,{col0,col1});
    // RelationValue r(home,d);

    // r.add(home,"a", "b");
    // r.add(home,"kate", "pete");

    // printRelation(home,r);
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Schema, comparison) {
  SpaceManager home;
  {
    Attribute col0(home, makeDomain(4));
    Attribute col1(home, makeDomain(4));
    Attribute col2(home, makeDomain(4));

    Schema rd0(home, {col0, col1, col2});
    Schema rd1(home, {col0});
    Schema rd2(home, {col1, col2});
    Schema rd3(home, {col0, col1, col2});

    // overlaps and sameAs
    EXPECT_TRUE(rd0.overlapsWith(rd1));
    EXPECT_FALSE(rd1.overlapsWith(rd2));
    EXPECT_FALSE(rd0.sameAs(rd1));
    EXPECT_TRUE(rd0.sameAs(rd3));

    // set operations on domains
    Schema rdCol0(home, {col0});
    Schema rdCol1(home, {col1});
    Schema rdCol2(home, {col2});

    // rdCol1 \cup rdCol2 = rd2
    Schema rdCol1rdCol2 = rdCol1.unionWith(home, rdCol2);
    EXPECT_TRUE(rdCol1rdCol2.sameAs(rd2));

    // rdCol0 \cup rdCol1 \cup rdCol2 = rd0
    Schema rdCol0Col1Col2 =
        rdCol0.unionWith(home, rdCol1).unionWith(home, rdCol2);
    EXPECT_TRUE(rdCol0Col1Col2.sameAs(rd0));

    // rdCol0 \ rdCol1 = rdCol0
    Schema diffRdCol0RdCol1 = rdCol0.differenceWith(home, rdCol1);
    EXPECT_TRUE(diffRdCol0RdCol1.sameAs(rdCol0));

    // rd0 \ rd1 = rd2
    Schema diffRd0Rd1 = rd0.differenceWith(home, rd1);
    EXPECT_TRUE(diffRd0Rd1.sameAs(rd2));

    // rd0 \ rd0 = empty
    Schema diffRd0Rd0 = rd0.differenceWith(home, rd0);
    EXPECT_TRUE(diffRd0Rd0.isEmpty());

    // rd0 \diffsym rd1 = rd2
    Schema diffsymRd0Rd1 = rd0.symDifferenceWith(home, rd1);
    EXPECT_TRUE(diffsymRd0Rd1.sameAs(rd2));

    Attribute col3(home, makeDomain(4));
    Schema rd4(home, {col1, col2, col3});
    Schema rd5(home, {col0, col3});

    // rd0 \simdiff rd4 = rd5
    Schema diffsymRd0Rd4 = rd0.symDifferenceWith(home, rd4);
    EXPECT_TRUE(diffsymRd0Rd4.sameAs(rd5));

    // todo: test contains, the Schema version!!
  }
  EXPECT_EQ(0, home.zeroReferences());
}
