#include <iostream>
#include <gtest/gtest.h>

#include "bdd-abstraction.hh"

using namespace std;
using namespace CuddAbstraction;

TEST(RelJoin, simpleJoin) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(4)), b(home, makeDomain(4)),
        c(home, makeDomain(4)), d(home, makeDomain(4));

    Schema domABC(home, {a, b, c});
    Schema domAB(home, {a, b});

    Relation plusABC = arithPlus(home, domABC);
    Relation ltAB = lessThan(home, domAB);

    /**
     * The relations that are joined have AB as common domains.
     *
     * The result will contain those tuples in the ground sum in which the
     * values under column A are less that the values under column B
     */
    Relation plusABCjoinLtAB = plusABC.joinWith(home, ltAB);
    EXPECT_EQ(plusABCjoinLtAB.arity(), 3u);

    Schema domCD(home, {c, d});
    Relation ltCD = lessThan(home, domCD);

    /**
     * The relations that are joined have disjoint domains
     *
     * This is the case in which the join is equivalent to a Cartesian product
     */
    Relation ltCDjoinLtAB = ltCD.joinWith(home, ltAB);
    EXPECT_EQ(ltCDjoinLtAB.arity(), 4u);
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(RelJoin, joinAndProject) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(8), "C0"), c1(home, makeDomain(8), "C1"),
        c2(home, makeDomain(8), "C2"), c3(home, makeDomain(8), "C3");

    Schema fd(home, {c0, c1, c2});

    Relation f(home, fd);
    f.add(home, {0, 3, 0});
    f.add(home, {0, 0, 1});
    f.add(home, {0, 2, 0});
    f.add(home, {7, 3, 0});

    Schema gd(home, {c0, c1, c3});

    Relation g(home, gd);
    g.add(home, {0, 0, 0});
    g.add(home, {0, 0, 1});
    g.add(home, {0, 2, 0});
    g.add(home, {7, 0, 0});


    Relation join = f.joinWith(home, g);

    Schema pSch(home, {c2, c3});
    Relation project = join.projectTo(home, pSch);
    // print(home, project, std::cout);

    Relation joinAndProject = f.joinWithAndProject(home,g, pSch);
    // print(home, joinAndProject, std::cout);

    EXPECT_TRUE(joinAndProject.sameAs(project));
  }
  EXPECT_EQ(0, home.zeroReferences());
}
