#include <iostream>
#include <gtest/gtest.h>

#include "bdd-abstraction.hh"

using namespace std;
using namespace CuddAbstraction;

TEST(Output, dot) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(16), "a"), b(home, makeDomain(16), "b"),
        c(home, makeDomain(16), "c");

    Schema dom(home, {a, b, c});
    Relation R(home, dom);
    R.add(home, 0, 2, 9);
    R.add(home, 1, 0, 9);
    R.add(home, 0, 3, 12);

    // Standard output printing
    print(home, R, std::cout);
    // DOT export
    R.toDot(home, "/tmp/out.dot", "G");
    std::cout << "Relation in dot file: /tmp/out.dot \n";
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Output, zeroArity) {
  SpaceManager home;
  {
    // Attribute emptyDom(home,makeDomain(0));
    Schema domr(home, {});

    Relation r = Relation::createEmpty(home, domr);

    // Trying to print an empty relation produces a warning
    print(home, r, std::cout);
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Output, formatter) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(16), "a"), b(home, makeDomain(16), "b"),
        c(home, makeDomain(16), "c");

    Schema dom(home, {a, b, c});
    Relation R(home, dom);
    R.add(home, 0, 2, 9);
    R.add(home, 1, 0, 9);
    R.add(home, 0, 3, 12);

    Formatter f(R, std::cout);
    f.print(home);

    std::ofstream out("/tmp/rel2.html");
    HtmlFormatter h(R, out);
    h.standalone(true);
    h.print(home);

    std::ofstream out1("/tmp/rel2.html");
    printHtml(home, R, out1);
  }
  EXPECT_EQ(0, home.zeroReferences());
}
