#include <iostream>
#include <gtest/gtest.h>

#include "bdd-abstraction.hh"
#include "format.h"

using namespace std;
using namespace CuddAbstraction;

TEST(Ground, plus) {
  SpaceManager home;
  {
    Attribute x(home, makeDomain(4)), y(home, makeDomain(4)),
        z(home, makeDomain(4));

    Schema dom(home, {x, y, z});

    Relation groundPlus = bitwisePlus(home, dom);
    // printRelation(home, groundPlus);

    Relation groundPlusOne = bitwisePlus(home, x, 1, z);
    // printRelation(home, groundPlusOne);

    Relation groundArithPlusOne = arithPlus(home, x, 1, z);
    // printRelation(home, groundArithPlusOne);

    Relation groundArithPlus = arithPlus(home, dom);
    // printRelation(home, groundArithPlus);
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Ground, plus4Bits) {
  SpaceManager home;
  {
    Attribute x(home, makeDomain(16)), y(home, makeDomain(16)),
        z(home, makeDomain(16));

    Schema dom(home, {x, y, z});

    Relation groundPlus = bitwisePlus(home, dom);
    // printRelation(home, groundPlus);

    Relation groundPlusThree = bitwisePlus(home, x, 3, z);
    // printRelation(home, groundPlusThree);

    Relation groundArithPlusThree = arithPlus(home, x, 3, z);
    // printRelation(home, groundArithPlusThree);

    Relation groundArithPlus = arithPlus(home, dom);
    // printRelation(home, groundArithPlus);
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Ground, interleavedPlus) {
  SpaceManager home;
  {
    // this simple test shows the difference between the variable order for the
    // same arithmetic relation plus.

    // Interleaved domains:
    Attribute x(home, makeDomain(16)), y(home, makeDomain(16)),
        z(home, makeDomain(16));

    Schema dom(home, {x, y, z});

    Relation groundArithPlus = arithPlus(home, dom);
    RelationStats s(groundArithPlus);

    // Linear domains:
    Attribute a(home, makeDomain(16), false), b(home, makeDomain(16), false),
        c(home, makeDomain(16), false);
    Schema dom2(home, {a, b, c});
    Relation other = arithPlus(home, dom2);
    RelationStats t(other);

    fmt::print("Density interleaved: {}, linear: {}\n", s.density(),
               t.density());

    EXPECT_EQ(groundArithPlus.cardinality(), other.cardinality());
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Ground, equality) {
  SpaceManager home;
  {

    // int maxElement = 65536;
    int maxElement = 16;
    // this simple test shows the difference between the variable order for the
    // same arithmetic relation plus.

    // Interleaved domains
    Attribute x(home, makeDomain(maxElement), "X"),
        y(home, makeDomain(maxElement), "Y");

    Schema dom(home, {x, y});

    Relation groundEquality = equal(home, dom);
    RelationStats s(groundEquality);

    // Linear domains
    Attribute a(home, makeDomain(maxElement), false),
        b(home, makeDomain(maxElement), false);
    Schema dom2(home, {a, b});
    Relation other = equal(home, dom2);
    RelationStats t(other);
    fmt::print("Density: (linear) {}, (interleaved) {}\n", s.density(),
               t.density());
    fmt::print("Nodes: (linear) {}, (interleaved) {}\n", s.nodes(), t.nodes());
    fmt::print("Cardinality: (linear) {}, (interleaved) {}\n", s.cardinality(),
               t.cardinality());

    // Export both BDDs in dot fomat
    groundEquality.toDot(home, "EqualityLinear.dot", "F");
    other.toDot(home, "EqualityInterleaved.dot", "F");

    EXPECT_EQ(groundEquality.cardinality(), other.cardinality());
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Ground, relational) {
  SpaceManager home;
  {
    Attribute x(home, makeDomain(4)), y(home, makeDomain(4)),
        z(home, makeDomain(4));

    Schema dom(home, {x, y});

    Relation groundLessThan = lessThan(home, dom);
    // printRelation(home, groundLessThan);

    Relation groundLessOrEqual = lessOrEqual(home, dom);
    // printRelation(home, groundLessOrEqual);

    Relation groundLessOrEqualCst = lessOrEqual(home, x, 2);
    // printRelation(home, groundLessOrEqualCst);

    Relation groundLessThanCst = lessThan(home, x, 2);
    // printRelation(home, groundLessThanCst);

    Relation groundGreaterThan = greaterThan(home, dom);
    // printRelation(home, groundGreaterThan);

    Relation groundGreaterOrEqual = greaterOrEqual(home, dom);
    // printRelation(home, groundGreaterOrEqual);

    Relation groundGreaterThanCst = greaterThan(home, x, 0);
    // printRelation(home, groundGreaterThanCst);

    Relation groundGreaterOrEqualCst = greaterOrEqual(home, x, 1);
    // printRelation(home, groundGreaterOrEqualCst);

    // Relation groundEqual = equal(home, dom);
    // printRelation(home, groundEqual);

    Relation groundDifferent = different(home, dom);
    // printRelation(home, groundDifferent);
  }
  EXPECT_EQ(0, home.zeroReferences());
}