#ifndef __ATTRIBUTE_HH__
#define __ATTRIBUTE_HH__

#include "attribute-decl.hh"
#include "format.h"

#include <algorithm>
#include <cassert>
#include <sstream>
#include <string>
#include <iterator>

namespace CuddAbstraction {

//////////////////
// Construction //
//////////////////
inline void Attribute::constructor(SpaceManager& home) {
  // TODO: Think about creating groups of variables using Cudd_MakeTreeNode
  // see:
  // http://www.async.ece.utah.edu/~myers/nobackup/ee5740_98/cudd/node18.html#secgroup

  // Whenever a domain is created, the corresponding bdd variables
  // are also created.
  for (auto v : indices_)
    home.addVar(v);

  // Create the cube corresponding to this domain
  cube_ = home.IndicesToCube(indices_.data(), indices_.size());
  assert(cube_.IsCube());
}

inline Attribute::Attribute(void) {}

inline Attribute::Attribute(SpaceManager& home, const std::vector<int>& vars,
                            AttributeDomain domain, const char* name)
    : domain_(domain)
    , indices_(home.alloc(vars))
    , name_(name) {
  assert(domain_);
  if (Limits::bitsRequiredToRepresent(domain_->max()) > vars.size())
    throw InadequateDomainRepresentation("Argsttribute::Attribute");

  constructor(home);
}

inline Attribute::Attribute(SpaceManager& home, AttributeDomain domain,
                            bool interleaved, const char* name)
    : domain_(domain)
    , indices_(home.alloc((domain_->maxCardinality() == 0)
                              ? 0
                              : Limits::bitsRequiredToRepresent(domain_->max()),
                          interleaved))
    , name_(name) {

  assert(domain_);
  constructor(home);
}

inline Attribute::Attribute(const Attribute& d)
    : domain_(d.domain_)
    , indices_(d.indices_)
    , cube_(d.cube_)
    , name_(d.name_) {
  // We do not create any BDD variable because we assume that the construction
  // of d already did that.
}

inline Attribute& Attribute::operator=(const Attribute& right) {
  if (this != &right) {
    indices_ = right.indices_;
    cube_ = right.cube_;
    name_ = right.name_;
    domain_ = right.domain_;
  }
  return *this;
}

////////////////////////
// Domain information //
////////////////////////
inline AttributeDomain Attribute::domain(void) const { return domain_; }

inline size_t Attribute::reprSize(void) const { return indices_.size(); }

inline const SpaceManager::BDDVarDomain& Attribute::getIndices(void) const {
  return indices_;
}

inline const std::string& Attribute::name(void) const { return name_; }

inline std::string Attribute::debugInfo(bool varInfo) const {
  fmt::MemoryWriter out;
  out.write("{{");
  out.write("\"name\": \"{}\"", name_);
  if (varInfo) {
    out.write(",\"vars\":[");
    for (size_t i = 0; i < indices_.size(); i++) {
      out.write("{}", indices_[i]);
      if (i < (indices_.size() - 1))
        out.write(",");
    }
    out.write("]");
  }
  out.write("}}");
  return out.str();
}

inline void Attribute::name(std::string n) { name_ = n; }

inline bool Attribute::hasSameAttributeDomainAs(const Attribute& other) const {
  return domain_ == other.domain_;
}

/////////////////////////
// Encoding / decoding //
/////////////////////////
inline BDD Attribute::asBDD(const SpaceManager& home,
                            const IntegerDatum& d) const {
  assert(indices_.size() >= d.numBits() &&
         "Not enough BDD variables to represent the provided datum");

  if (!domain_->valid(d))
    throw InvalidDomainData("Attribute::asBDD(IntegerDatum)");

  // The loop below encodes the integer d as a BDD using the variables of
  // the domain. The i-th bit of the number is encoded using the i-th
  // variable of the domain.
  BDD result = home.bddOne();
  for (size_t i = 0; i < indices_.size(); i++) {
    if (d.getBit(i))
      result &= home.bddVar(indices_[i]);
    else
      result &= !home.bddVar(indices_[i]);
  }

  return result;
}

inline IntegerDatum Attribute::asInteger(/*const SpaceManager& home,*/
                                         int* minterm) const {
  IntegerDatum d(0);
  for (size_t i = 0; i < reprSize(); i++) {
    if (minterm[i] == 1)
      d.setBit(i);
    else {
      assert(minterm[i] == 0);
    }
  }
  return d;
}

inline std::vector<int> Attribute::select(int* minterm) const {
  std::vector<int> result;
  result.reserve(reprSize());
  for (auto i : getIndices())
    result.push_back(minterm[i]);
  return result;
}

////////////
// Output //
////////////

inline void
Attribute::fillWithBDDVarNames(std::vector<std::string>& names) const {
  for (const auto& i : indices_) {
    std::stringstream ss;
    ss << name_ << ":" << i;
    names[i] = ss.str();
  }
}

// namespace internal {
// inline void
// initializeDomain(SpaceManager& home, int card, int index,
//                  const std::vector<SpaceManager::BDDVarDomain>& domains) {}
// template <typename... Args>
// void initializeDomain(SpaceManager& home, int card, int index,
//                       const std::vector<SpaceManager::BDDVarDomain>& domains,
//                       Attribute& domain, Args&... args) {
//   // @a domain is assigned to a new value domain using the variables
//   specified
//   //     by @a domains[index].
//   domain = Attribute(home, domains[index], makeDomain(card));
//   initializeDomain(home, card, index + 1, domains, args...);
// }
// }

// template <typename... Args>
// void initializeDomains(SpaceManager& home, int card, Args&... args) {

//   if (card != 0 && !Limits::valid(card - 1))
//     throw InvalidDomainData("initilizeAttributes");

//   std::vector<SpaceManager::BDDVarDomain> domains =
//       home.alloc(sizeof...(Args), Limits::bitsRequiredToRepresent(card - 1));
//   internal::initializeDomain(home, card, 0, domains, args...);
// }

inline std::ostream& operator<<(std::ostream& os, const Attribute& d) {
  os << "'" << d.name() << "'";
  return os;
}

//////////////////////////
// Attribute operations //
//////////////////////////
inline BDD Attribute::asCube(void) const { return cube_; }

inline std::vector<BDD>
Attribute::asVectorOfVariables(const SpaceManager& home) const {
  std::vector<BDD> result(reprSize(), home.bddOne());
  for (size_t i = 0; i < reprSize(); i++) {
    auto idx = indices_[i];
    result[i] = home.bddVar(idx);
  }
  return result;
}

inline bool Attribute::operator==(const Attribute& d) const {
  return cube_ == d.cube_;
}

inline bool Attribute::operator!=(const Attribute& d) const {
  return !(cube_ == d.cube_);
}

inline bool Attribute::operator<(const Attribute& a) const {
  // The correctness of this operation is based on the fact that the BDD manager
  // allocates new sets of variables every time.
  return indices_[0] < a.indices_[0];
}

inline bool sameReprSize(const Attribute& d, const Attribute& e) {
  return d.reprSize() == e.reprSize();
}

inline bool sameReprSize(const Attribute& d, const Attribute& e,
                         const Attribute& f) {
  return sameReprSize(d, e) && sameReprSize(e, f);
}
}

#endif
