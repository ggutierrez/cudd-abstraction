#ifndef __DOMAIN_DECL_HH__
#define __DOMAIN_DECL_HH__

#include <memory>
#include "integer-datum-decl.hh"

namespace CuddAbstraction {

class BaseDomain {
private:
  size_t max_cardinality_;

public:
  /// Default constructor
  BaseDomain(void);
  /// Constructor for an empty domain that can hold up to @a size elements
  BaseDomain(const IntegerDatum& size);
  /// Copy constructor
  BaseDomain(const BaseDomain& d) = delete;
  /// Assignment operator
  BaseDomain& operator=(const BaseDomain&) = delete;
  /// Destructor
  ~BaseDomain(void);
  /// Returns the maximum cardinality of the domain
  size_t maxCardinality(void) const;
  /// Returns the maximum element of the domain
  int max(void) const;
  /// Tests whether @a v is a valid domain encoding in this domain
  bool valid(int v) const;
  /// Returns the width of the maximum element of this domain
  size_t printWidth(void) const;
};

/**
 * @brief A domain can be shared by several attributes.
 *
 * Conceptually, the domain only contains the values that an attribute an take
 * as values.
 *
 * As a domain can be shared by several attributes in different relations we use
 * a shared pointer.
 */
typedef std::shared_ptr<BaseDomain> AttributeDomain;

/**
 * @brief Creates a new domain of cardinality @a card
 */
AttributeDomain makeDomain(int card);
}

#endif