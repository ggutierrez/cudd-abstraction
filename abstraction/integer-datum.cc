#include "integer-datum.hh"

char* encodeInteger(int d) {
  using namespace CuddAbstraction;
  IntegerDatum id(d);
  std::string enc = id.encoding();
  char* result = (char*)malloc(sizeof(char) * enc.size() + 1);
  std::copy(enc.begin(), enc.end(), result);
  result[enc.size()] = '\0';
  return result;
}